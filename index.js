String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

var checkID = function(s) {
    s = s.replaceAll("-", "");
    if (s.length != 11) {
        return false
    }
    var n = 0;
    for (var i = 0; i < s.length; i++) {
        if (s.length - 1 != i) {
            n += (s.charCodeAt(i) - 23);
        }
    }
    n = n.toString();

    return n[n.length - 1] == s[s.length - 1];
}

function makeID(dashes) {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    var n = 0;
    for (var i = 0; i < text.length; i++) {
        n += (text.charCodeAt(i) - 23);
    }
    n = n.toString();
    console.log(n, n[n.length - 1]);

    text += n[n.length - 1];

    if (dashes) {
        text = text.replace(/(.{3})/g, "$1-");
    }

    return text;
}

checkID("6kc-mrg-o77-50") //true
checkID("491wvvea3w1"); //true
checkID("a4h9jkfe248"); //false
